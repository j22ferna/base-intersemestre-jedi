# Base code for the JEDI robots

This repository contains the squeleton of the code for running the robot. 
It deals with the initialisation of the sensors and the motors throught the RPi.GPIO library.
You also have access to a manual controller of the robot where you can control the movement with the keyboard and the terminal.

## Missing pieces

The goal of the intersemestre is to implement the missing pieces of code throughout the module. 
The missing functions are marked with a `TODO` comment.
Mainly, these functions are needed to :
* control the modulation sent to the motor, according to the data received from the sensors;
* explore the maze, by avoiding the walls and moving from intersection to intersection until the robot found the exit; and
* build a graph representation of the maze and move throught the maze again, with knowledge.

# Fork

This repository is a clean slate and should not be modified by the students.
They should fork the project onto their own repository.
