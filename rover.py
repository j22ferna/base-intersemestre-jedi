"""
File: rover.py
Description: Entry point for running the robot from the keyboard
"""

from droids import Droid3Sensors

# rover = RoverDroid()
r2d2 = Droid3Sensors()

# Calibrate
r2d2.mv_duration = 6
r2d2.sp_duration = 3

wall_dist = 50  # max range
danger_dist = 9 # crash at 6

r2d2.front_sensor.sensor.low = danger_dist
r2d2.left_sensor.sensor.low = danger_dist
r2d2.right_sensor.sensor.low = danger_dist
r2d2.front_sensor.sensor.high = wall_dist
r2d2.left_sensor.sensor.high = wall_dist
r2d2.right_sensor.sensor.high =  wall_dist

lo = r2d2.front_sensor.sensor.low
hi = r2d2.front_sensor.sensor.high
print("Wall dist (HIGH) =", hi, "| Danger dist (LOW) =", lo)

# rover.run()
r2d2.explore()

